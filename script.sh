#!/bin/bash

# On Debian/Ubuntu: sudo apt-get install imagemagick webp
cd ./source || exit

# Change to the desired max Height or Width
# resizeTo=864 # Max width on the weall.net and showittomenow.com blogs.
resizeTo=4096

lossless() {
  if [ "$1" == "--lossless" ]; then
    lossless_param=' -q 90 '
  else
    lossless_param=''
  fi
}

lossless "$@"

for image in ./*.jpg; do
  if [[ ! -e $image ]]; then
    continue
  fi
  tempFilename=${image#*./}
  size=($(identify -format '%w %h' $image))
  if [ ${size[0]} -gt ${size[1]} ]; then
    if [ ${size[0]} -gt $resizeTo ]; then
      cwebp $lossless_param -resize $resizeTo 0 -mt $image -o ../output/${tempFilename%.*}.webp
    else
      cwebp $lossless_param -mt $image -o ../output/${image%.*}.webp
    fi
  else
    if [ ${size[1]} -gt $resizeTo ]; then
      cwebp $lossless_param -resize 0 $resizeTo -mt $image -o ../output/${tempFilename%.*}.webp
    else
      cwebp $lossless_param -mt $image -o ../output/${tempFilename%.*}.webp
    fi
  fi
  # rm $image
  echo ${image%.*}
done

for image in ./*.png; do
  if [[ ! -e $image ]]; then
    continue
  fi
  tempFilename=${image#*./}
  size=($(identify -format '%w %h' $image))
  if [ ${size[0]} -gt ${size[1]} ]; then
    if [ ${size[0]} -gt $resizeTo ]; then
      cwebp $lossless_param -resize $resizeTo 0 -mt $image -o ../output/${tempFilename%.*}.webp
    else
      cwebp $lossless_param -mt $image -o ../output/${tempFilename%.*}.webp
    fi
  else
    if [ ${size[1]} -gt $resizeTo ]; then
      cwebp -$lossless_param resize 0 $resizeTo -mt $image -o ../output/${tempFilename%.*}.webp
    else
      cwebp $lossless_param -mt $image -o ../output/${tempFilename%.*}.webp
    fi
  fi
  # rm $image
done

for image in ./*.webp; do
  if [[ ! -e $image ]]; then
    continue
  fi
  tempFilename=${image#*./}
  size=($(identify -format '%w %h' $image))
  if [ ${size[0]} -gt ${size[1]} ]; then
    if [ ${size[0]} -gt $resizeTo ]; then
      cwebp $lossless_param -resize $resizeTo 0 -mt $image -o ../output/${tempFilename%.*}.webp
    else
      cwebp $lossless_param -mt $image -o ../output/${image%.*}.webp
    fi
  else
    if [ ${size[1]} -gt $resizeTo ]; then
      cwebp $lossless_param -resize 0 $resizeTo -mt $image -o ../output/${tempFilename%.*}.webp
    else
      cwebp $lossless_param -mt $image -o ../output/${tempFilename%.*}.webp
    fi
  fi
  # rm $image
  echo ${image%.*}
done
