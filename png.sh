#!/bin/bash

# On Debian/Ubuntu: sudo apt-get install imagemagick webp
cd ./source || exit

# Change to the desired max Height or Width
# resizeTo=864 # Max width on the weall.net and showittomenow.com blogs.
resizeTo=1250

for image in ./*.jpg; do
  if [[ ! -e $image ]]; then
    continue
  fi
  tempFilename=${image#*./}
  size=($(identify -format '%w %h' $image))
  if [ ${size[0]} -gt ${size[1]} ]; then
    if [ ${size[0]} -gt $resizeTo ]; then
      convert $image -size $resizeTo -quality 90% ../output/png/${tempFilename%.*}.png
    else
      convert $image -quality 90% ../output/png/${image%.*}.png
    fi
  else
    if [ ${size[1]} -gt $resizeTo ]; then
      convert $image -size $resizeTo -quality 90% ../output/png/${tempFilename%.*}.png
    else
      convert $image -quality 90% ../output/png/${tempFilename%.*}.png
    fi
  fi
  # rm $image
  echo ${image%.*}
done

for image in ./*.jpg; do
  if [[ ! -e $image ]]; then
    continue
  fi
  tempFilename=${image#*./}
  size=($(identify -format '%w %h' $image))
  if [ ${size[0]} -gt ${size[1]} ]; then
    if [ ${size[0]} -gt $resizeTo ]; then
      convert $image -size $resizeTo -quality 90% ../output/jpg/${tempFilename%.*}.jpg
    else
      convert $image -quality 90% ../output/jgp/${image%.*}.jpg
    fi
  else
    if [ ${size[1]} -gt $resizeTo ]; then
      convert $image -size $resizeTo -quality 90% ../output/jpg/${tempFilename%.*}.jpg
    else
      convert $image -quality 90% ../output/jpg/${tempFilename%.*}.jpg
    fi
  fi
  # rm $image
  echo ${image%.*}
done
