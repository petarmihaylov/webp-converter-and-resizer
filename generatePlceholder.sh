#!/bin/bash

# Change to the desired max Height or Width
# resizeTo=864 # Max width on the weall.net and showittomenow.com blogs.
width=1920
height=1080

numImagesToGenerate=20
extension=jpg

# Delete the image if it exists
if [[ -f ./tempPlaceholderImage.png ]]; then
  rm tempPlaceholderImage.png
fi

if [[ -d ./placeholderImages ]]; then
  echo "creating placeholderImages folder"
  rm -rf placeholderImages
  mkdir placeholderImages
else
  mkdir placeholderImages
  echo "creating placeholderImages folder"
fi

curl "https://dummyimage.com/${width}x${height}/fff/aaa" -o tempPlaceholderImage.png

for ((i = 1; i <= numImagesToGenerate; i++)); do
  cwebp -mt tempPlaceholderImage.png -o ./placeholderImages/${i}.${extension}
  echo "./placeholderImages/${i}.${extension} created."
done

cwebp -mt tempPlaceholderImage.png -o ./placeholderImages/banner.${extension}
echo "./placeholderImages/banner.${extension} created."

cwebp -mt tempPlaceholderImage.png -o ./placeholderImages/spotlight1.${extension}
echo "./placeholderImages/banner.${extension} created."

cwebp -mt tempPlaceholderImage.png -o ./placeholderImages/spotlight2.${extension}
echo "./placeholderImages/banner.${extension} created."

cwebp -mt tempPlaceholderImage.png -o ./placeholderImages/spotlight3.${extension}
echo "./placeholderImages/banner.${extension} created."
