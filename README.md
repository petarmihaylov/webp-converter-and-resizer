# Webp Convert and Resize

## Introduction

Just a quick script to convert and resizing JPG, PNG, and WEBP images. Edit the script directly to configure the desired behavior. Comments in the script file provide guidance for each option.

The script will go recursively through all the PNG, JPG, and WEBP files in the `./source` folder and compare the width of the image to the `resizeTo` variable value. If the with of the image is larger, the image will be resized to the specified `resizeTo` width keeping the image's aspect ratio.

The script is intended to run on Linux / Mac OS only, however, you can use Windows Subsystem Linux (WSL) to install the prerequisites on an Ubuntu based WSL installation on windows.

## Prerequisites

The script requires `imagemagick` and `webp`.

### Debian/Ubuntu

```
sudo apt-get install imagemagick webp
```

### Brew

```
brew install imagemagick webp
```

## Usage

1. Add all images you would like to convert in the `./source` folder. Do not add subfolders.
2. Edith the `./script.sh` file and change `resizeTo` variable to the desired value.
3. Save the changes.
4. `./script.sh`
5. Done!
